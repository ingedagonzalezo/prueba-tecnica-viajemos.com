<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

//Autores
Route::get('Autores', 'AutorController@index')
    ->name('paginas.autores');

Route::get('/ReadAutores', [
    'as' => 'ReadAutores',
    'uses' => 'AutorController@ReadAutores',
]);

Route::post('/CreateAutor', [
    'as' => 'CreateAutor',
    'uses' => 'AutorController@CreateAutor',
]);

Route::post('/UpdateAutor', [
    'as' => 'UpdateAutor',
    'uses' => 'AutorController@UpdateAutor',
]);

//Editoriales
Route::get('Editoriales', 'EditorialController@index')
    ->name('paginas.editoriales');

Route::get('/ReadEditoriales', [
    'as' => 'ReadEditoriales',
    'uses' => 'EditorialController@ReadEditoriales',
]);

Route::post('/CreateEditorial', [
    'as' => 'CreateEditorial',
    'uses' => 'EditorialController@CreateEditorial',
]);

Route::post('/UpdateEditorial', [
    'as' => 'UpdateEditorial',
    'uses' => 'EditorialController@UpdateEditorial',
]);

//Libros
Route::get('Libros', 'LibroController@index')
    ->name('paginas.libros');

Route::get('/ReadLibros', [
    'as' => 'ReadLibros',
    'uses' => 'LibroController@ReadLibros',
]);

Route::post('/CreateLibro', [
    'as' => 'CreateLibro',
    'uses' => 'LibroController@CreateLibro',
]);

Route::post('/UpdateLibro', [
    'as' => 'UpdateLibro',
    'uses' => 'LibroController@UpdateLibro',
]);
