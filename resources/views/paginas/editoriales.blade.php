@extends('welcome')

@section('pageTitle', ' - Editoriales')

@section('content')
<div class="card" style="margin-top: 15px">
    <h5 class="card-header">Editoriales</h5>
    <div class="card-body">
        <div id="grid">

        </div>
    </div>
</div>
@stop
@section('scripts_adicionales')
<script>
    $(document).ready(function () {
        
                
            var crudServiceBaseUrl = "{{asset('/')}}";

                                      
                        dataSource = new kendo.data.DataSource({
                            transport: {
                                read:  {
                                    url: crudServiceBaseUrl + "ReadEditoriales",
                                    dataType: "json"
                                },
                                update: {
                                    url: crudServiceBaseUrl + "UpdateEditorial",
                                    dataType: "json",                    
                                    type: "POST"
                                },
                                create: {
                                    url: crudServiceBaseUrl + "CreateEditorial",
                                    dataType: "json",                    
                                    type: "POST"
                                },
                                parameterMap: function(options, operation) {
                                    if (operation !== "read" && options.models) {
                                       return {
                                            datos: kendo.stringify(options.models),
                                            _token: "{!! csrf_token()!!}"
                                        };
                                    }
                                }
                            },
                            batch: true,
                            pageSize: 20,
                            schema: {
                                 model: {
                                    id: "id",
                                    fields: {
                                        id: { editable: false, nullable: true },
                                        nombre: { validation: { required: {message: "El nombre de la editorial es requerido"} } },
                                        sede: {validation: { required: {message: "La sede de la editorial es requerido"} }},
                                        }
                                    }
                                }
                        });
       

                    $("#grid").kendoGrid({
                        dataSource: dataSource,
                        pageable:{
                            refresh: true,
                            pageSizes: true,
                            buttonCount: 5
                        },
                        toolbar: ["create", "search"],
                        groupable: true,
                        filterable: true,
                        sortable: true,
                        columns: [
                            { 
                                field:"nombre",
                                title: "Nombre Editorial", 
                                width: "200px"
                            },
                            {
                                field: "sede",
                                title: "Sede Editorial",
                                width: "200px"
                            },
                            { command: ["edit"], title: "&nbsp;", width: "250px" }],
                        editable: "popup"
                    });
                    $('.k-input').attr('placeholder', "Buscar");
                });
</script>
@stop