@extends('welcome')

@section('pageTitle', ' - Libros')

@section('content')
<div class="card" style="margin-top: 15px">
    <h5 class="card-header">Libros</h5>
    <div class="card-body">
        <div id="grid">

        </div>
    </div>
</div>
@stop
@section('scripts_adicionales')
<script>
    $(document).ready(function () {        
                
            var crudServiceBaseUrl = "{{asset('/')}}";


            var dsEditoriales = new kendo.data.DataSource({
                            transport:{
                                read:{
                                    url: crudServiceBaseUrl + "ReadEditoriales",
                                    dataType: "json"
                                }
                            },
                            batch: true,
                            pageSize: 30,

                            schema: {
                                model: {
                                    id: "id",
                                    fields: {}
                                }
                            }
                        });
                        dsEditoriales.read();
       
                                      
                        dataSource = new kendo.data.DataSource({
                            transport: {
                                read:  {
                                    url: crudServiceBaseUrl + "ReadLibros",
                                    dataType: "json"
                                },
                                update: {
                                    url: crudServiceBaseUrl + "UpdateLibro",
                                    dataType: "json",                    
                                    type: "POST"
                                },
                                create: {
                                    url: crudServiceBaseUrl + "CreateLibro",
                                    dataType: "json",                    
                                    type: "POST"
                                },
                                parameterMap: function(options, operation) {
                                    if (operation !== "read" && options.models) {
                                       return {
                                            datos: kendo.stringify(options.models),
                                            _token: "{!! csrf_token()!!}"
                                        };
                                    }
                                }
                            },
                            batch: true,
                            pageSize: 20,
                            schema: {
                                 model: {
                                    id: "id",
                                    fields: {
                                        id: { editable: false, nullable: true },
                                        titulo: { validation: { required: {message: "El título del libro es requerido"} } },
                                        sinopsis: { validation: { required: {message: "La sinopsis es requerida"} } },
                                        n_paginas: {validation: { required: {message: "El número de paginas es requerido"}} },
                                        }
                                    }
                                }
                        });

                        

                    $("#grid").kendoGrid({
                        dataSource: dataSource,
                        pageable:{
                            refresh: true,
                            pageSizes: true,
                            buttonCount: 5
                        },
                        toolbar: ["create", "search"],
                        groupable: true,
                        filterable: true,
                        sortable: true,
                        columns: [
                            { 
                                field:"titulo",
                                title: "Título del Libro", 
                                width: "200px"
                            },
                            { 
                                field:"sinopsis",
                                title: "Sinopsis", 
                                width: "200px"
                            },                            
                            {
                                field: "editoriales",
                                title: "Editorial",
                                width: "200px",
                                template: function (dataItem) {
                                    var editorial = dsEditoriales.data().find(x => x.id == dataItem.idEditorial);
                                    if(editorial != undefined){
                                        var nombre = kendo.htmlEncode(editorial.nombre);
                                        return nombre;
                                    }else{
                                        return "";
                                    }
                                },
                                editor: function (container, options) {

                                        var input = $("<input>");

                                        input.attr("name", options.field);

                                        input.appendTo(container);


                                        var cmb = input.kendoDropDownList({
                                            optionLabel: "Selecione...",
                                            dataTextField: "nombre",
                                            dataValueField: "id",
                                            dataSource: dsEditoriales,
                                            index: 0
                                        }).data('kendoDropDownList');

                                        setTimeout(function(){
                                            cmb.value(options.model.idEditorial);
                                        }, 250);
                                        
                                        console.log(options);

                                    },
                                
                                    
                            },
                            { command: ["edit"], title: "&nbsp;", width: "250px" }],
                        editable: "popup",
                        edit: function(e){
                            var ddl = e.container.find('[data-role=dropdownlist]').data('kendoDropDownList');
                            if(ddl){ 
                                console.log(ddl.dataSource.read(),"entro");
                                ddl.dataSource.read();
                            }
                        }
                    });
                    $('.k-input').attr('placeholder', "Buscar");
                });
</script>
@stop