@extends('welcome')

@section('pageTitle', ' - Autores')

@section('content')
<div class="card" style="margin-top: 15px">
    <h5 class="card-header">Autores</h5>
    <div class="card-body">
        <div id="grid">

        </div>
    </div>
</div>
@stop
@section('scripts_adicionales')
<script>
    $(document).ready(function () {
        
                
            var crudServiceBaseUrl = "{{asset('/')}}";

                                      
                        dataSource = new kendo.data.DataSource({
                            transport: {
                                read:  {
                                    url: crudServiceBaseUrl + "ReadAutores",
                                    dataType: "json"
                                },
                                update: {
                                    url: crudServiceBaseUrl + "UpdateAutor",
                                    dataType: "json",                    
                                    type: "POST"
                                },
                                create: {
                                    url: crudServiceBaseUrl + "CreateAutor",
                                    dataType: "json",                    
                                    type: "POST"
                                },
                                parameterMap: function(options, operation) {
                                    if (operation !== "read" && options.models) {
                                       return {
                                            datos: kendo.stringify(options.models),
                                            _token: "{!! csrf_token()!!}"
                                        };
                                    }
                                }
                            },
                            batch: true,
                            pageSize: 20,
                            schema: {
                                 model: {
                                    id: "id",
                                    fields: {
                                        id: { editable: false, nullable: true },
                                        nombres: { validation: { required: {message: "Los nombres del autor es requerido"} } },
                                        apellidos: { validation: { required: {message: "Los apellidos del autor es requerido"} } },
                                        }
                                    }
                                }
                        });
       

                    $("#grid").kendoGrid({
                        dataSource: dataSource,
                        pageable:{
                            refresh: true,
                            pageSizes: true,
                            buttonCount: 5
                        },
                        toolbar: ["create", "search"],
                        groupable: true,
                        filterable: true,
                        sortable: true,
                        columns: [
                            { 
                                field:"nombres",
                                title: "Nombres", 
                                width: "200px"
                            },
                            {
                                field: "apellidos",
                                title: "Apellidos",
                                width: "200px"
                            },
                            { command: ["edit"], title: "&nbsp;", width: "250px" }],
                        editable: "popup"
                    });
                    $('.k-input').attr('placeholder', "Buscar");
                });
</script>
@stop