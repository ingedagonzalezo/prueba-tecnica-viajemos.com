<!--Kendo-->
<script src="{{ asset('assets/kendo/js/jszip.min.js')}}"></script>
<script src="{{ asset('assets/kendo/js/kendo.all.min.js')}}"></script>
<script src="{{ asset('assets/kendo/js/cultures/kendo.culture.es-ES.min.js')}}"></script>
<script src="{{ asset('assets/kendo/js/messages/kendo.messages.es-ES.min.js')}}"></script>


<script>
    kendo.culture("es-ES");
</script>