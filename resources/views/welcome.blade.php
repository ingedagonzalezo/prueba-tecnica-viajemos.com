<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
        <title>Prueba Tecnica PHP - @yield('pageTitle')</title>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ URL::asset('assets/font-awesome/css/all.css')}}">

        <!--Kendo-->
        <link rel="stylesheet" href="{{ URL::asset('assets/kendo/styles/kendo.common-material.min.css')}}" />
        <link rel="stylesheet" href="{{ URL::asset('assets/kendo/styles/kendo.material.min.css')}}" />
        <link rel="stylesheet" href="{{ URL::asset('assets/kendo/styles/kendo.material.mobile.min.css')}}" />
    </head>

    <body>

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Prueba Tecnica PHP - </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('paginas.autores')}}">Autores <span
                                class="sr-only"></span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('paginas.editoriales')}}">Editoriales</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('paginas.libros')}}">Libros</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div style="padding: 10px 10px 10px 10px">
            @yield('content')

        </div>


        <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
        <script src="assets/assets/js/libs/jquery-3.1.1.min.js"></script>
        <script src="assets/bootstrap/js/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>

        @include('partials.scripts')@yield('scripts_adicionales')

    </body>

</html>