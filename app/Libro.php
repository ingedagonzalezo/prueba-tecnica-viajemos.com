<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    protected $table = 'libros';

    public function Editoriales()
    {
        return $this->hasOne('App\Editorial', 'id', 'idEditorial');
    }

    public function Autores()
    {
        return $this->belongsToMany("Autor");
    }
}
