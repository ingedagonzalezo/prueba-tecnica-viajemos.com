<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libro;
use App\Editorial;
use App\Autor;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LibroController extends Controller
{
    public function index()
    {
        return view('paginas.libros');
    }

    public function ReadLibros()
    {
        $l = Libro::orderby('titulo', 'asc')->get();

        return (new Response($l, 200));
    }

    public function CreateLibro(Request $request)
    {
        //die($request);

        $datos = json_decode($request->datos);

        //die(print_r($datos));
        $l = new Libro();

        $l->titulo = $datos[0]->titulo;
        $l->sinopsis = $datos[0]->sinopsis;
        $l->n_paginas = $datos[0]->n_paginas;
        $l->idEditorial = $datos[0]->editoriales->id;
        $l->save();

        return (new Response($l, 200));
    }

    public function UpdateLibro(Request $request)
    {
        //die($request);    
        $datos = json_decode($request->datos);

        
        //die(print_r($datos));
        
        $l = Libro::find($datos[0]->id);

        $l->titulo = $datos[0]->titulo;
        $l->sinopsis = $datos[0]->sinopsis;
        $l->n_paginas = $datos[0]->n_paginas;
        $l->idEditorial = $datos[0]->editoriales->id;
        $l->save();

        return (new Response($l, 200));
    }
}
