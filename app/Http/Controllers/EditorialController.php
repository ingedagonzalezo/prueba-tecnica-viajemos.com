<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Editorial;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class EditorialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    public function index()
    {
        return view('paginas.editoriales');
    }

    public function ReadEditoriales()
    {
        $e = Editorial::orderby('nombre', 'asc')->get();
        return (new Response($e, 200));
    }

    public function CreateEditorial(Request $request)
    {
        //die($request);

        $datos = json_decode($request->datos);

        //die(print_r($datos));
        $e = new Editorial();

        $e->nombre = $datos[0]->nombre;
        $e->sede = $datos[0]->sede;
        $e->save();

        return (new Response($e, 200));
    }

    public function UpdateEditorial(Request $request)
    {
        //die($request);

        $datos = json_decode($request->datos);

        //die(print_r($datos));
        $e = Editorial::find($datos[0]->id);

        $e->nombre = $datos[0]->nombre;
        $e->sede = $datos[0]->sede;
        $e->save();

        return (new Response($e, 200));
    }

    public function DestroyAutor(Request $request)
    {
        //die($request);

        $datos = json_decode($request->datos);

        //die(print_r($datos));
        $a = Autor::find($datos[0]->id);
        $a->delete();

        return (new Response($a, 200));
    }
}
