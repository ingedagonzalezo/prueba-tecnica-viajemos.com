<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Autor;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AutorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    public function index()
    {
        return view('paginas.autores');
    }

    public function ReadAutores()
    {
        $a = Autor::orderby('nombres', 'asc')->get();
        return (new Response($a, 200));
    }

    public function CreateAutor(Request $request)
    {
        //die($request);

        $datos = json_decode($request->datos);

        //die(print_r($datos));
        $a = new Autor();

        $a->nombres = $datos[0]->nombres;
        $a->apellidos = $datos[0]->apellidos;
        $a->save();

        return (new Response($a, 200));
    }

    public function UpdateAutor(Request $request)
    {
        //die($request);

        $datos = json_decode($request->datos);

        //die(print_r($datos));
        $a = Autor::find($datos[0]->id);

        $a->nombres = $datos[0]->nombres;
        $a->apellidos = $datos[0]->apellidos;
        $a->save();

        return (new Response($a, 200));
    }
}
